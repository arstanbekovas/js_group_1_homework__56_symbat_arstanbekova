import React, { Component } from 'react';

import Controller from "./Controller";

import Burger from "../Components/Burger";


class BurgerBuilder extends Component{
    state = {
        cheese: [],
        salad: [1],
        meat: [],
        bacon: []
    };

    addIng = (ing) => {
       let ingradients = [...this.state[ing]];
       ingradients.push(1);
       this.setState({
           [ing]: ingradients
       })
    };

    removeIng = (ing) => {
        let ingradients = [...this.state[ing]];
        ingradients.pop();
        this.setState({
            [ing]: ingradients
        })

    };

    render() {
        return (
            <div className='Burger'>

                <Controller
                    inc = {() => this.addIng('cheese')}
                    dec = {() => this.removeIng('cheese')}
                    ing = 'cheese' />
                <Controller
                    inc = {() => this.addIng('salad')}
                    dec = {() => this.removeIng('salad')}
                    ing = 'salad' />
                <Controller
                    inc = {() => this.addIng('meat')}
                    dec = {() => this.removeIng('meat')}
                    ing = 'meat'/>
                <Controller
                    inc = {() => this.addIng('bacon')}
                    dec = {() => this.removeIng('bacon')}
                    ing = 'bacon'/>
                <Burger ings = {this.state}/>

            </div>


        )
    }


}


export default BurgerBuilder;

