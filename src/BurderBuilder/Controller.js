import React from 'react';

const Controller = props => {
    return (
        <div>
            <button onClick={props.inc}> + {props.ing} </button>
            <button onClick={props.dec}> - {props.ing} </button>
        </div>

    )
};

export default Controller;