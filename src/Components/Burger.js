import React from 'react';
import BreadTop from "./BreadTop";
import Seeds1 from "./Seeds1";
import Seeds2 from "./Seeds2";
import BreadBottom from "./BreadButtom";
import Salad from "./Salad";
import Cheese from "./Cheeese";
import Bacon from "./Bacon";
import Meat from "./Meat";


const Burger = props => {

        return (

            <div className="Burger">
                <BreadTop>
                    <Seeds1/>
                    <Seeds2/>
                </BreadTop>
                {props.ings.salad.map(s => {
                    return <Salad/> }
                )}
                {props.ings.cheese.map(c => {
                    return <Cheese/> }
                )}
                {props.ings.bacon.map(b => {
                    return <Bacon/> }
                )}
                {props.ings.meat.map(m => {
                    return <Meat/> }
                )}
                <BreadBottom/>
            </div>
        )

};


export default Burger;